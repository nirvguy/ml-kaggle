import pandas as pd

def clean_dataset(df):
    del df['Name']
    del df['Cabin']
    del df['PassengerId']
    del df['Ticket']
    df['Age'] = df['Age'].fillna(df['Age'].mean())
    df['Fare'] = df['Fare'].fillna(df['Fare'].mean())
    df['Embarked'] = df['Embarked'].fillna(df['Embarked'].value_counts().index[0])
